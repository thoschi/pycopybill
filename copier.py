from selenium import webdriver
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import Select


import json
import csv
import os
from datetime import datetime # for logging
import time                   # for waiting between download attempts
import os.path                # to check if device/driver file exists

class dev():
   '''
   this class represents a copier device
   it basically consists of
   - a devprops-dictionary containing all necessary informations to access a device
   - a drvprops-dictionary containing all model-specific informations
   - a drvactions-dictionary containing model-dependant commands
   
   the class offers a convenient interface to a copiers web interface using python/selenium
   
   variables inside commands are dynamically substituted and there is a status-system that avoids
   unnecessary actions within the web interface
   
   following informations are at least expected inside the "device" dictionary:
   - model (used to identify the correct driver)
   - address (str, the ip-address/hostname to connect to the web interface)
   - port (int, the port to connect to the web interface)
   - ssl (boolean, if connection is secured - used for url creation
   - id/password (str, credentials for admin access)
   
   the "driver" dictionary consists of (colon-separated) pairs of selenium-based actions.
   So far the following commands are available for driver actions:
   - desc: (just a description - does nothing)
   - urlget: (go to the specified url)
   - xpclick: (click on the item specified by xpath)
   - jsrun: (run the specified javascript-function)
   - xpgetinput: (retrieve value of an input field specified by xpath)
   - xpsetinput: (set values of an input field specified by xpath
   - xpgetdropdownval: (retrieves the dropdown values of a dropdown specified by xpath)
   - xpparsetable: (returns a twodimensional list representing the table at specified xpath)
   - action: (call another action)
   - replace: (replace string in result before returned)
   
   there is also a dynamic status-system that allows to refer to different required or
   provided statuses (eg "logged in" or "settings"). This avoids unnecessary clicks within the
   virtual selenium driver:
   - requests:<statusname>  (indicates a status requested - only continues, when status is reached)
   - setstatus:<statusname> (sets a certain status)
   - provides:<statusname>  (action that provides a certain status - should only be part of one action for each status)
   
   inside the values of commands (behind the colon) variables in double dollar signs $$id$$ are dynamically substituted:
   - first inside **kwargs
   - then from the devprops-dict
   - last from the drvprops-dict
   if tags still remain afterwards the action is cancelled (to avoid faulty actions)   
   
   actions return a (dict, errorcode) tuple, where the dict contain values to keys provided in the command itself
   the keyword for return values can be set by the command by providing the variable name in the end of the command (indicated by ->)
   
   included is the possibility to deal with csv-files (as it might be the most convenient way to
   evaluate user page counters by evaluating a csv file. for this reason some additional commands are available:
   
   
   often there are different ways to get an actual user list with counters:
   - a csv export (best)
   - an (often paginated) html table
   - details page for each user
   the device class will report or accept
   - a user object with user information (to set or read)
   - a list of user objects with all users and the corresponding informations
   the class will automatically detect, which way to get/set users (depending on which
   way is available in the corresponding printer class)
   '''
   def __init__(self, devid, drvid, *, loaddev=True, loaddrv=True, headless=True, debug=False, **kwargs):

      # set debug to true to enable extensive output
      self.debug = debug
      if self.debug: print("PyCopyBill started at", datetime.now().strftime("%d.%m.%Y %H:%M:%S"))
      if self.debug: print("* debug output enabled")


      self.devid           = devid   # a unique device id (also used as a filename for the dev file)
      self.drvid           = drvid   # a unique driver id (also used as a filename for the drv file)

      self.status          = None    # place holder for the device status (initially "None" = not even logged in)
      
      self.recursion       = 0       # the actual recursion level
      self.maxrecursion    = 5       # allowed recursion depth to avoid circular command definitions
      
      self.tmpfolder       = '/tmp/' # needed for file downloads
      self.dlretries       = 10      # wait retries until downloads finished
      self.dlwait          = 1       # wait time between trying to read file

      # loading device/driver if possible/wanted, otherwise empty
      self.devprops        = {}
      self.drvprops        = {}
      self.drvactions      = {}
      if loaddev: self.loaddev()
      if loaddrv: self.loaddrv()

      # selenium browser setup
      self.browser_timeout = 20

      service = Service(os.path.dirname(__file__)+r'/tools/geckodriver')
      options=Options()

      #options.binary_location = 
      
      #profile_path = r'C:\Users\Administrator\AppData\Roaming\Mozilla\Firefox\Profiles\y1uqp5mi.default'
      #options.set_preference('profile', profile_path)

      options.set_preference("browser.download.folderList", 2)
      options.set_preference("browser.download.manager.showWhenStarting", False)
      options.set_preference("browser.download.dir", self.tmpfolder)
      options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/x-gzip")
      options.set_preference("browser.helperApps.neverAsk.openFile", "application/octet-stream")
      options.set_preference("browser.helperApps.neverAsk.openFile", "application/octet-stream")
      options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream")

      # set headless to false in selenium/ff options to see what selenium is actually doing
      options.headless = headless
      print("* headless set to", options.headless) if self.debug else ""

      self.browser = Firefox(service=service, options=options)

   def __del__(self):
      self.browser.quit()

   def __mkurl(self, pg):
      '''create urls (basically adding http/s and Port)'''
      if 'addr' and 'ssl' and 'port' in self.devprops and pg != '':   # test for invalid urls
         url = 'http'                                                 # start with http
         if self.devprops['ssl']: url += 's'                          # adding s for ssl connections
         url += '://' + self.devprops['addr']                         # add basic address
         if self.devprops['port']: url += ':' + self.devprops['port'] # add port
         url += pg                                                    # add page provided
         if self.debug: print("* url created:", url)
         return url
      else:
         return False

   def __vreplace(self, cmd, **kwargs):
      '''this internal method replaces the variables in command strings (included in $$...$$)'''
      known = {**self.devprops, **self.drvprops, **kwargs} # create a dictionary of all known variables
      vsplit = cmd.split('$$')                             # split the command into commands and variables
      if len(vsplit) > 1:                                  # every second occurance is a variable then and is replaced if possible
         for i in range(1, len(vsplit), 2):                # iterate over variables
            if vsplit[i] in known:                         # test if variable is available
               vsplit[i] = known[vsplit[i]]                # replace
      res = ''.join(vsplit)                                # rejoin parts to command string
      return res if not '$$' in res else False             # check if all Variables have been replaced

   def do(self, key, **kwargs):
      retval, retcode = {}, 0               # the empty return value for "get" functions
      self.recursion += 1                   # add recursion depth
      if self.recursion > self.maxrecursion:
         if self.debug: print("X maximum of recursions reched")
         return (False, 'MAX_RECURSION')
      for cmd in self.drvactions[key]:         # evaluate all commands inside action
         
         a = self.__vreplace(cmd, **kwargs) # replace variables before command execution
         k, v = a.split(':', 1)             # splits command and arguments

         # the requests-command marks a status that is requested before execution
         if k == 'requests':
            if self.status != v:
               #if self.debug: print("* calling tostatus", v,"with args", kwargs)
               retcode = self.tostatus(v, **kwargs)

         # the provides-command marks a status that is provided by its action
         elif k == 'provides':
            self.status = v

         elif k == 'desc':
            pass

         elif k == 'urlget':
            self.browser.get(self.__mkurl(v))

         # wait for an element to be loaded
         elif k == 'xpwaitfor':
            WebDriverWait(self.browser, self.browser_timeout).until(ec.presence_of_element_located((By.XPATH, v)))

         # click on an element given by its xpath
         elif k == 'xpclick':
            WebDriverWait(self.browser, self.browser_timeout).until(ec.presence_of_element_located((By.XPATH, v))).click()

         # click on an element given by its xpath AND accept a following alert
         elif k == 'xpclickalert':
            WebDriverWait(self.browser, self.browser_timeout).until(ec.presence_of_element_located((By.XPATH, v))).click()
            alert = WebDriverWait(self.browser, self.browser_timeout).until(ec.alert_is_present())
            alert.accept()

         # run a javascript-command
         elif k == 'jsrun':
            if self.debug: print("* jsrun called for script", v)
            self.browser.execute_script(v)

         # sum up variables
         elif k == 'varsum':
            if self.debug: print("* varsum called for variables", v)
            summands, result = v.split('->')
            retval[result] = sum([int(retval[i]) for i in summands.split(';')])


         # downloads a csv file and saves the content as list of lists inside the csv-key of the return dict
         # command is issued by getcsv:<downloadtype>:<script-name/xpath-to-button>:<filename>:<keys(#->name)>
         elif k == 'getcsv':
            dltype, xp, filename, keys = v.split(':', 3) # the 
            f = os.path.join(self.tmpfolder, filename)
            self.csv = None                              # a temporary storage for csv files
            if os.path.isfile(f): os.remove(f)           # remove file to avoid copies
               
            fexists = False
            # file might be downloaded by a javascript-execution of a button click
            if dltype == 'jsrun':
               print('jsrun, mache', v)
               self.browser.execute_script(xp)
            elif dltype == 'xpclick':
               WebDriverWait(self.browser, self.browser_timeout).until(ec.presence_of_element_located((By.XPATH, xp))).click()
            for r in range(0, self.dlretries):
               if os.path.isfile(os.path.join(self.tmpfolder, filename)):
                  with open(os.path.join(self.tmpfolder, filename)) as csvfile:
                     c = list(csv.reader(csvfile, delimiter=',', quoting=csv.QUOTE_NONE))
                  #retval = list(xyz)
                  os.remove(os.path.join(self.tmpfolder, filename))
                  break
               else:
                  #retval = None
                  time.sleep(1)
            # extract the needed tables
            retval['csv'] = []
            for r in c:
               if not r == []:
                  d = {}
                  for k in keys.split(';'):
                     d[k.split('->')[1]] = r[int(k.split('->')[0])]
                  retval['csv'].append(d)

         # basic modifications to the imported csv data, allows comma separated list
         elif k == 'csvmod':
            jobs = v.split(',')
            if retval != None:
               for j in jobs:
                  if j == '-header':
                     retval['csv'].pop(0)
                  elif j == '-footer':
                     retval['csv'].pop()
                  elif j == 'del':
                     retval.pop('csv', None)

         # get limit of a user
         elif k == 'xpgetinput':
            xp, key = v.split('->', 1)
            f = WebDriverWait(self.browser, self.browser_timeout).until(ec.presence_of_element_located((By.XPATH, xp)))
            retval[key] = f.get_attribute('value')
            
         # sets an input field to a given text
         elif k == 'xpsetinput':
            # extract value to set from v
            text, xp = v.split(':', 1)
            if text != "":    # do not allow for empty strings
               f = WebDriverWait(self.browser, self.browser_timeout).until(ec.presence_of_element_located((By.XPATH, xp)))
               if f.get_attribute('value') != text:
                  f.clear()      # remove old value
                  f.send_keys(text) # enter new value

         # sets a checkbox to "checked"
         elif k == 'xpenablecb':
            cbstate = WebDriverWait(self.browser, self.browser_timeout).until(ec.visibility_of_element_located((By.XPATH, v))).is_selected()
            if not cbstate: WebDriverWait(self.browser, self.browser_timeout).until(ec.visibility_of_element_located((By.XPATH, v))).click()
         
         elif k == 'xpstatuscb':
            xp, key = v.split('->', 1)
            retval[key] = WebDriverWait(self.browser, self.browser_timeout).until(ec.visibility_of_element_located((By.XPATH, xp))).is_selected()
         
         # get value of a dropdown
         elif k == 'dropdownval':
            l = Select(WebDriverWait(self.browser, self.browser_timeout).until(ec.presence_of_element_located((By.XPATH, v))))
            #l = Select(self.browser.find_element(By.ID, self.drv.id_pages))
            #l = Select(self.browser.find_element(By.XPATH, v))
            retval = [i.get_attribute('value') for i in l.options]
         
         # get the alt-attribute of an image element provided by an xpath
         elif k == 'xpgetimgalt':
            xp, key = v.split('->', 1)
            retval[key] = WebDriverWait(self.browser, self.browser_timeout).until(ec.visibility_of_element_located((By.XPATH, xp))).get_attribute("alt")
         
         # get the value of an input field
         elif k == 'xpgetinputval':
            xp, key = v.split('->', 1)
            retval[key] = WebDriverWait(self.browser, self.browser_timeout).until(ec.visibility_of_element_located((By.XPATH, xp))).get_attribute("value")
         
         # TODO parses a table within the remote ui
         elif k == 'xpparsetable':
            ''' some information inside drv.props might be needed for parsing a table
                - self.table_pag = True # whether the table is paginated (the number of pages need to be set then)
                - self.xp_pages  = ''   # xpath of the pagination dropdown
                - self.id_pages  = ''   # id of the pagination dropdown ($$$ this seems doubled somehow)
                - self.pg_id     = ''   # url of department page ($p is replaced by actual page)
                - self.xp_table  = ''   # xpath of the counters-table
            '''
            # parse a table and return as (two dimensional) list
            counters = []
            WebDriverWait(self.browser, self.browser_timeout).until(ec.presence_of_element_located((By.XPATH, v)))
            xp_tbody = v + '/tbody'
            num_rows = len (self.browser.find_elements(By.XPATH, xp_tbody + "/tr"))
            num_cols = len (self.browser.find_elements(By.XPATH, xp_tbody + "/tr[2]/td"))
            for row in range(1, (num_rows + 1)):
               c = []
               for col in range(1, num_cols + 1):
                 xp_td = xp_tbody + "/tr[" + str(row) + "]/td[" + str(col) + "]"
                 c.append(self.browser.find_element('xpath', xp_td).text.replace("\n",""))
               counters.append(c)
            retval = counters

         # manually set a status (helpful to avoid returning to a status)
         elif k == 'setstatus':
            if self.debug: print("* status auf", v, "gesetzt")
            self.status = v
         
         # get the text at a given xpath
         elif k == 'xpgettext':
            xp, key = v.split('->', 1)
            
            retval[key] = WebDriverWait(self.browser, self.browser_timeout).until(ec.presence_of_element_located((By.XPATH, xp))).text

         # TODO - replace text inside a key of the retval-dictionary
         elif k == 'replace':
            # will replace in strings, in lists of strings and lists of lists of strings
            if retval != None:
               s, r = v.split(':', 1)
               #ld = listidh(retval)
               if ld == 0:
                  newval = retval.replace(s,r)
               elif ld == 1:
                  newval = [ e.replace(s,r) for e in retval ]
               elif ld == 2:
                  newval = [ [e.replace(s,r) for e in f] for f in retval ]
               retval = newval

         # execute another action
         elif k == 'action':
            v, c = do(v)      # get results of another do
            retval.update(v)  # add return dictionary TODO!!! no idea what happends by this
            retcode = c

      self.recursion -= 1     # reduce recursion level
      if retcode == 0:
         return retval, 0
      else:
         return {}, retcode

   def tostatus(self, status, **kwargs):
      ''' find and call the action, that provides the requested status'''
      retcode = 0
      if self.debug: print("* tostatus from", self.status, "to", status)

      for t in self.drvactions:
         for c in self.drvactions[t]:
            if self.__vreplace(c, **kwargs) == 'provides:'+ status:
               if self.debug: print("  found in", t)
               _, retcode = self.do(t, **kwargs)
      return retcode
               

   def counters(self):
      if self.drvprops['pagination']:
         counter = []
         pages, err = self.do('getpages')
         if err == 0:
            print("  Got", len(pages), "pages!:", pages)
            for p in pages:
               print("  Go to page", p)
               counter += self.__do('getcounter', pagenum = p)[0]
         else:
            print("FEHLER:", pages[0])
      else:
         return self.__do('getcounter')
      
   
   def getdrvprops(self):
      if self.debug: print("* get all driver properties")
      return self.drvprops
      
   def getdrvprop(self, key):
      if self.debug: print("* get driver property", key)
      return self.drvprops[key]

   def getactions(self):
      if self.debug: print('* get all driver actions')
      return self.drvactions
      
   def getdrvaction(self, key):
      if self.debug: print('* get driver action', key)
      return self.drvactions[key]

   def setdrvprop(self, key, value):
      if self.debug: print("* set driver property", key, "to", value)
      self.devprops[key] = value
      
   def setdrvaction(self, key, value):
      if self.debug: print("* set driver action", key, "to", value)
      self.drvactions[key] = value

   def deldrvprop(self, key):
      if self.debug: print("* delete driver property", key)
      self.drvprops.pop(key)

   def deldrvaction(self, key):
      if self.debug: print("* delete driver action", key)
      self.drvactions.pop(key)
         
   def commands(self, action):
      '''return a numbered list of all commands inside an action'''
      if action in self.drvactions:
         res = [ (i, list(self.drvactions[action])[i]) for i in range(0, len(self.drvactions[action])) ]
      else:
         res = False
      return res
   
   def addcommand(self, action, command, position = False):
      '''add a command to an action (optional at a certain position)'''
      if not position or position > len(self.drvactions[action]): 
         position = len(self.drvactions[action]) # if no explicit position is set, append
      self.drvactions[action].insert(position, command)
      return True

   def getdevprops(self):
      return self.devprops
      
   def getdevprop(self, key):
      return self.devprops[key]

   def getactions(self):
      return self.drvactions
      
   def getaction(self, key):
      return self.drvactions[key]

   def setdevprop(self, key, value):
      self.devprops[key] = value
   
   def deldevprop(self, key):
      self.devprops.pop(key)

   # ~ def delaction(self, key):
      # ~ self.drvactions.pop(key)

   # import/export
   def loaddrv(self):
      '''load driver json'''
      filename = os.path.dirname(__file__)+'/'+self.drvid + ".drv"
      if self.debug: print("* loading", filename)
      if os.path.exists(filename):
         try:
            with open(filename, 'r') as f:
               [ self.drvprops, self.drvactions ] = json.load(f)
         except:
            print("FILE ERROR")
      #d = Driver(model=model, props=json.dumps(props),actions=json.dumps(actions))

   def savedrv(self):
      '''export the device-specific jsons to a file'''
      filename = self.drvid + ".drv"
      with open(filename, 'w') as f:
         f.write(json.dumps([self.drvprops, self.drvactions]))

   def loaddev(self):
      '''load device json'''
      filename = os.path.dirname(__file__)+'/'+self.devid + ".dev"
      if self.debug: print("* loading", filename)
      if os.path.exists(filename):
         try:
            with open(filename, 'r') as f:
               [ self.devprops ] = json.load(f)
         except:
            print("FILE ERROR")
      #d = Driver(model=model, props=json.dumps(props),actions=json.dumps(actions))

   def savedev(self):
      '''save device information to dev-file (devid.dev)'''
      filename = self.devid + ".dev"
      with open(filename, 'w') as f:
         f.write(json.dumps([self.devprops]))
      
   def action(self, action, **kwargs):
      return self.do(action, **kwargs)

   def logout():
      pass
   def resetcounter():
      pass
   

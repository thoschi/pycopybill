#!/usr/bin/python3
# - !./venv/bin/python

import argparse # well... thats the sense of the cli
import os       #
import sys      #
import csv      # csv2limit

from copier import dev

# Create the parser
pcb_ap = argparse.ArgumentParser(prog='PyCopyBill', description='PyCopyBill Command Line Interface')

# Add the arguments

pcb_ap.add_argument('-dev',
                    '--device',
                    metavar='Kopierer-ID',
                    type=str,
                    help='ID des Kopierers',
                    required=True)

pcb_ap.add_argument('-drv',
                    '--driver',
                    metavar='Kopierer-Modell',
                    type=str,
                    help='Modell des Kopierers',
                    required=True)


pcb_ap.add_argument('-uid',
                    '--userid',
                    metavar='Benutzer-ID',
                    type=str,
                    help='ID des zu bearbeitenden Nutzers',
                    required=False)

pcb_ap.add_argument('-lbw',
                    '--limitbw',
                    metavar='Limit(sw)',
                    type=str,
                    help='Schwarzweiß-Limit für Nutzer',
                    required=False)

pcb_ap.add_argument('-lcl',
                    '--limitcl',
                    metavar='Limit(cl)',
                    type=str,
                    help='Farb-Limit für Nutzer',
                    required=False)

pcb_ap.add_argument('-f',
                    '--filename',
                    metavar='Dateiname',
                    type=str,
                    help='Name der CSV-Datei (für csv2limit)',
                    required=False)


pcb_ap.add_argument('command',
                    metavar='Befehl',
                    type=str,
                    help='auszuführender Befehl')

# Execute the parse_args() method
args = pcb_ap.parse_args()

dev1 = dev(args.device, args.driver, headless=True)

if args.command == "getcsv":
   retval, retcode = dev1.action('getcounters')
   #print("uid;countercl;counterbw;")
   for line in retval['csv']:
      print(line['uid']+";"+line['countercl']+";"+line['counterbw']+";")

elif args.command == "setlimit":
   retval, retcode = dev1.action('setlimit', uid=args.userid, limitbw=args.limitbw, limitcl=args.limitcl)
   # ~ retval, retcode = dev1.action('getlimit', uid=args.userid)
   # ~ if retval['limitbw'] != args.limitbw or retval['limitcl'] != args.limitcl or retval['checkbw'] == False or retval['checkcl'] == False:
      # ~ retval, retcode = dev1.action('setlimit', uid=args.userid, limitbw=args.limitbw, limitcl=args.limitcl)
   # ~ else:
      # ~ retval, retcode = dev1.action('provides', status='settings')
      # ~ retval, retcode = dev1.action('userlist')
   print("Ergebnis (setlimit):", retval)

elif args.command == "resetcounters":
   retval, retcode = dev1.action('resetcounters')

elif args.command == "getsumcounter":
   retval, retcode = dev1.action('getsumcounter')
   print(retval['sumcounter'])

elif args.command == "csv2limit":
   with open(args.filename, newline='') as csvfile:
      csvdata = csv.reader(csvfile, delimiter=';')
      for row in csvdata:
         retval, retcode = dev1.action('setlimit', uid=row[0], limitbw=row[1], limitcl=row[2])

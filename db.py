from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float
Base = declarative_base()


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    username = Column(String)
    password = Column(String)
    balance = Column(Float(3))

    def __repr__(self):
        return f'User {self.name}'

class Transactions(Base):
    __tablename__ = 'transactions'
    id = Column(Integer, primary_key=True)
    user = Column(Integer)
    amount = Column(Float(3))
    transactiontype = Column(Integer)
    information = Column(String)

class TransactionTypes(Base):
    '''can be debit, charge or marker, others as needed'''
    __tablename__ = 'ttypes'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    shortname = Column(String)

class Uids(Base):
    '''the user accounts are the printer IDs - can be grouped (for accounting rules) and are connected to user ids'''
    __tablename__ = 'uids'
    id = Column(Integer, primary_key=True)
    uid = Column(String) # might usually be numeric but let's keep it general
    uidgroup = Column(Integer)
    user = Column(Integer)
    
    
class UidGroups(Base):
    __tablename__ = 'uidgroups'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    shortname = Column(String)

class JobTypes(Base):
    '''the types define, what kind of jobs exists and can be issued. printers may "offer" different of these jobs
    example: bw / colour'''
    __tablename__ = 'jobtypes'
    id = Column(Integer, primary_key=True)
    desc = Column(String)
    baseprice = Column(Float(3))
    pageprice = Column(Float(3))

class MapCounter(Base):
    '''for each jobtype and each device map a column for counter and limit'''
    __tablename__ = 'mapcounter'
    id = Column(Integer, primary_key=True)
    jobtype = Column(Integer) # id of the corresponding jobtype
    device = Column(Integer)  # id of the device
    column = Column(Integer)  # column of the according counter in the devices counter table

class MapLimit(Base):
    '''list of devices'''
    __tablename__ = 'maplimit'
    id = Column(Integer, primary_key=True)
    jobtype = Column(Integer) # id of the corresponding jobtype
    device = Column(Integer)  # id of the device
    column = Column(Integer)  # column of the according counter in the devices counter table
    

class Devices(Base):
    ''' any device may offer any jobtype'''
    __tablename__ = 'devices'
    id = Column(Integer, primary_key=True)
    address = Column(String)  # hostname/ip of the device
    location = Column(String) # location of the device
    driver = Column(String)   # driver modle id of the device

class Driver(Base):
    '''database storage of the (json) device strings'''
    __tablename__ = 'driverjson'
    id = Column(Integer, primary_key=True)
    model = Column(String)       # a readable name for this printer model
    props = Column(String)       # device specific property-json
    actions = Column(String)     # device specific action-json
    
class Settings(Base):
    '''some persistent settings'''
    __tablename__ = 'driver'
    id = Column(Integer, primary_key=True)
    key = Column(String)     # any settings key
    val = Column(String)     # any settings value


engine = create_engine('sqlite:///:memory:', echo=False)
Session = sessionmaker(bind=engine)
session = Session()

Base.metadata.create_all(engine)

#user = User(name='John Snow', password='johnspassword')
#session.add(user)

#session.commit()
#print(user.id)  # None

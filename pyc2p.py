# todo
# find a way to check if still logged in (should not be a big issue, as usually the script will not pause long enough)
# load all drivers to a drvstore


from copier import dev
from db import session, User, Transactions, TransactionTypes, Uids, UidGroups, JobTypes, Mapping, Devices, Driver, Settings
import random, string
import json

# deal with errors (users not found, counters not correct)


class CoPrAS:
    '''contains all accounting and management functionality'''

    def __init__(self):
        # settings
        self.accounttype = 0 # 0 = numeric, 1 = chars, 2 = alphanumeric
        self.accountlen = 6 # length of the account ids
        self.accountmaxtries = 20 # when to give up trying to produce unique ids

    # common methos
    def get(self, item, column):
        return [getattr(i,column) for i in session.query(item).all()]


    # user methods
    def add_user(self, name, username):
        '''manually add a user (and create the ids and sync it to the devices)'''
        # add user if not exists
        if session.query(User).filter_by(name=username).count() == 0:
            u = User(name=name, username=username)
            session.add(u)
            session.commit()
            
            # create id's for user
            self.sync_uids()
            # create user on devices
            self.sync_devices()       


    def del_user(self, **kwargs):
        '''manually delete a user from devices after final billing.'''
        q = None
        if 'id' in kwargs:
            q = session.query(User).filter_by(id=kwargs['id']).first()
        elif 'username' in kwargs:
            q = session.query(User).filter_by(username=kwargs['name']).first()
        elif 'uid' in kwargs:
            u = session.query(Uids).filter_by(uid=kwargs['uid']).first().user
            q = session.query(User).filter_by(id=u).first()
        if q != None:
            session.delete(q)
            session.commit() 

    def informuser(self, username):
        '''send messages to users if limit reached'''

    # uid methods
    def add_uidgroup(self, name, shortname):
        '''add an accountgroup (and create the corresponding ids for all users and add them to all devices)'''
        ug = UidGroups(name=name, shortname=shortname)
        session.add(ug)
        session.commit()
        self.sync_uids()

    def del_uidgroup(self, **kwargs):
        '''removes an uid group
        todo: backup information'''
        q = None
        if 'id' in kwargs:
            q = session.query(UidGroups).filter_by(id=kwargs['id']).first()
        elif 'name' in kwargs:
            q = session.query(UidGroups).filter_by(name=kwargs['name']).first()
        elif 'shortname' in kwargs:
            q = session.query(UidGroups).filter_by(shortname=kwargs['shortname']).first()
        if q != None:
            session.delete(q)
            session.commit() 
            self.sync_uids()

    def __create_unique_uid(self):
        for i in range(0, self.accountmaxtries):
            if self.accounttype == 0:
                newid = ''.join(random.choices(string.digits, k=self.accountlen))
            elif self.accounttype == 1:
                newid = ''.join(random.choices(string.ascii_uppercase, k=self.accountlen))
            elif self.accounttype == 2:
                newid = ''.join(random.choices(string.ascii_uppercase + string.digits, k=self.accountlen))
            # check if id is unique
            if session.query(Uids).filter_by(id=newid).count() == 0:
                return newid
        return None

    def sync_uids(self):
        '''make sure that every user has uids for all uidgrp and
        creates/deletes uids as needed'''
        # iterate users
        for u in session.query(User):
            gq = [q.id for q in session.query(UidGroups)] # query all uid groups ids
            uq = [q.uidgroup for q in session.query(Uids).filter_by(user=u.id)] # query all uid groups of uids of the user
            create = [e for e in gq if e not in uq] # existing group ids not in user uids
            delete = [e for e in uq if e not in gq] # existing users group uids without corresponding uid group
            for c in create:
                u = Uids(uid = self.__create_unique_uid(), uidgroup = c, user = u.id) # create a unique new id
                session.add(u)
                session.commit()
            for d in delete:
                q = session.query(Uids).filter_by(user=u.id, uidgroup = d).first() # delete uid
                session.delete(q)
                session.commit()   
        
    # devices
    def import_driver(self, model, filename):
        '''adds driver-jsons for a certain model to the database
        todo: check to avoid different drivers of same model'''
        with open(filename, 'r') as f:
            [ props, actions ] = json.load(f)
        d = Driver(model=model, props=json.dumps(props),actions=json.dumps(actions))
        session.add(d)
        session.commit()
        
        
    def export_driver(self, model, filename):
        '''export the device-specific jsons to a file'''
        with open(filename, 'w') as f:
            f.write(json.dumps([self.props, self.actions]))
      
    
    def add_device(self, address, location, driver):
        '''adds a new device to the database'''
        d = Devices(address=address, location=location, driver=driver)
        session.add(d)
        session.commit()
    
    def del_device(self, **kwargs):
        d = None
        if 'id' in kwargs:
            q = session.query(Devices).filter_by(id=kwargs['id']).all()
        elif 'address' in kwargs:
            q = session.query(User).filter_by(username=kwargs['name']).all()
        elif 'driver' in kwargs:
            q = session.query(Uids).filter_by(uid=kwargs['uid']).all()
            
        if q != None:            
            for i in q:
                session.delete(i)
            session.commit() 

    def sync_uids(self):
        '''synchronize the department id's with all devices'''
        # get list of all uids
        uids = [ i.uid for i in session.query(Uids).all() ]
        print(uids)
        
        # get devices (for loop)
        
            # fetch ids from devices
            
            # users to add
            
            # users to delete
            
    def set_limits(self, **kwargs):
        '''set the limits for all users (or certain users if passed in kwargs)'''
        # make sure you have a list of all users
        if 'user' in kwargs:
            if not isinstance(kwargs['user'], list):
                u = [ kwargs['user'] ]
        # iterate all devices
        q = session.query(Devices).all()
        for j in q:
            # create 
            for i in u:
            
                    
        
        
        
    def processdevices(self):
        '''read counters, bill users, reset counters and set new pagelimits'''
        pass
    
    
    # transactions
    def add_transactiontype(self, name, shortname):
        '''add a transaction type'''
        tt = TransactionTypes(name=name, shortname=shortname)
        session.add(tt)
        session.commit()

    def del_transactiontype(self, name, shortname):
        '''delete a transaction type'''
        pass 
    
    def deposit(self, user, amount):
        '''add a deposit to a user'''
        
        pass
    
    def charge(self, id, device, ddd):
        '''dont know yet'''
        pass
    
    def gethistory(self):
        '''get history of transactions (filtered by username, amount, ...)'''
        pass
    
    # jobs
    def add_jobtype(self, desc, baseprice, pageprice):
        '''adds a jobtype'''
        j = JobTypes(desc = desc, baseprice = baseprice, pageprice = pageprice)
        session.add(j)
        session.commit
        
    def deljobtype(self, name):
        '''deletes a jobtype'''
        
        
    def setmappings(self):
        '''sets the mapping of jobtype/device/counter-column'''
        pass

    def getmapping(self):
        '''gets a mapping'''
        pass
    
    # others
    def sanitize(self):
        '''check the health of the whole system'''
        # check users for email, balance
        
        # check, if all users have all ids
        
        # check if all users exists on all devices
        
        
        # check if user balances are valid
        
        # check if counters are fine
        pass
    
class App:
    def __init__(self):
        copras = CoPrAS()
        
        # create some example data
        copras.add_user('Thomas Schröder', 'shoe')
        copras.add_user('Christoph Schütz', 'sue')
        
        copras.add_uidgroup('Privat', 'p')
        copras.add_uidgroup('Dienstlich', 'd')
        
        copras.add_transactiontype('Aufladung','charge')
        copras.add_transactiontype('Umbuchung','transfer')
        copras.add_transactiontype('Markierung','marker')
        copras.add_transactiontype('Belastung','debit')
        
        copras.import_driver('test','c2.drv')
        
        copras.add_device('localhost', 'Schule', 1)
        
        
        q = session.query(Driver).filter_by(id=1).first()
        props, actions = json.loads(q.props), json.loads(q.actions)
        
        d1 = dev(props=props, actions=actions)
        d1.tostatus(2)
        
        for u in copras.get(User,'id'):
            print('Benutzer', session.query(User).filter_by(id=u).first().name, 'mit UIDs', [m.uid for m in session.query(Uids).filter_by(user=u).all()])
        test = session.query(Uids).filter_by(user=1).first().uid
        print('HIER',test)
        copras.del_user(uid=test)
        #print(copras.get(UidGroups, 'name'))

        for u in copras.get(User,'id'):
            print('Benutzer', session.query(User).filter_by(id=u).first().name, 'mit UIDs', [m.uid for m in session.query(Uids).filter_by(user=u).all()])
        
        print(copras.get(UidGroups, 'name'))
        #print(copras.get(User, 'name'))

        copras.add_jobtype('Farbe', 0, 0.6)
        copras.add_jobtype('Schwarzweiß', 0, 0.15)

            
        while True:
            module = input('Was tun? B)enutzerverwaltung, K)opiererabrechnung, S)ystemverwaltung, X) Verlassen').lower().strip()
            if module == 'b':
                username = input('Kürzel angeben:').lower().strip()
                # existiert ja/nein
                q = session.query(User).filter_by(username=username)
                if q.count() == 0:
                    print('Neuer Benutzer?')
                    name = input("Namen eingeben: ")
                    if name != "":
                        username = input('Benutzernamen eingeben')
                        if username != "":
                            copras.adduser(name, username)
                elif q.count() == 1:
                    uids = session.query(Uids).filter_by(user=q.first().id)
                    print('ist da und hat die Nummern', [u.uid for u in uids])
                    
                elif q.count() > 1:
                    print('aua')
                                    
                # Kontostand anzeigen
                # was tun
                action = input('E)inzahlen, M)ailadresse ändern, H)istory ausgeben, L)öschen, X) Verlassen').lower().strip()
            elif module == 'k':
                # Kopierer abrechnen
                # Limits prüfen/setzen
                # erreichbarkeit prüfen
                # Gerätehistory
                pass
            elif module == 's':
                # alles anzeigen
                while True:
                    aglist = []
                    for i in session.query(UidGroups).all():
                        aglist.append(i.shortname+' ('+i.name+')')
                    print('Aktuelle Accountgruppen:', ', '.join(aglist))
                    action = input('Accountgruppe E)rstellen, Accountgrupp L)öschen, X) Verlassen').lower().strip()
                    if action == 'e':
                        aglname = input('Lesbarer Name der Accountgruppe: ')
                        agsname = input('Kurzname der Accountgruppe: ')
                        copras.add_uidgroup(aglname, agsname)
                    elif action == 'x':
                        break
                    # JobType hinzufügen/löschen
                    # Geräte löschen/hinzufügen
                    # mapping bearbeiten
            elif module == 'x':
                break


        



app = App()



